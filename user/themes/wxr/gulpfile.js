var browserify = require("browserify");
var buffer     = require("vinyl-buffer");
var gulp       = require("gulp");
var less       = require("gulp-less");
var rename     = require("gulp-rename");
var source     = require("vinyl-source-stream");
var sourcemaps = require("gulp-sourcemaps");
var uglify     = require("gulp-uglify");

gulp.task("default", ["scripts", "styles"]);

gulp.task("scripts", function () {
    return browserify("scripts/src/index.js").bundle()
        .pipe(source("index.js"))
        .pipe(buffer())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(rename({ extname: ".min.js" }))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest("scripts/dist"))
    ;
});

gulp.task("styles", function () {
    return gulp.src("styles/src/index.less")
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(rename({ extname: ".min.css" }))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest("styles/dist"))
    ;
});

gulp.task("watch", function () {
    gulp.watch("scripts/src/*.js", ["scripts"]);
    gulp.watch("styles/src/*.less", ["styles"]);
});
