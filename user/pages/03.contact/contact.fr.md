---
title: Contact
form:
    name: contact
    method: POST
    fields:
        -   name: civility
            label: ~
            type: radio
            validate:
                required: true
            options:
                mr:  M.
                mrs: Mme.
        -   name: lastname
            label: Nom
            type: text
            validate:
                required: true
        -   name: firstname
            label: Prénom
            type: text
            validate:
                required: true
        -   name: company
            label: Société
            type: text
            validate:
                required: true
        -   name: address
            label: Adresse
            type: text
        -   name: postal_code
            label: NPA
            type: text
        -   name: city
            label: Localité
            type: text
        -   name: phone
            label: Téléphone
            type: text
        -   name: email
            label: E-mail
            type: email
            validate:
                required: true
                email: true
        -   name: message
            label: Remarque
            type: textarea
            rows: 5
            validate:
                required: true
        -
            name: g-recaptcha-response
            label: Captcha
            type: captcha
            recatpcha_site_key: ij3eoij3oirj3oiejio3wjeioje
            recaptcha_not_validated: 'Captcha not valid!'
            validate:
                required: true
---

Pour tous renseignements complémentaires, merci de prendre contact avec nous à l'aide du formulaire ci-contre.

...

Ou alors ce formulaire ne sert à rien. Je ne sais pas.

...

Tu peux aussi directement écrire à [nique@les-poules.org](mailto:nique@les-poules.org)
