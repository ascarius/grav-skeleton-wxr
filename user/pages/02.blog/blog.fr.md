---
title: Blog
content:
    items: @self.children
    order:
        by: date
        dir: desc
    limit: 5
    pagination: true
pagination: true
---
